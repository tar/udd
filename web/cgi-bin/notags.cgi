#!/usr/bin/python3
# -*- coding: utf-8; mode: python; tab-width: 4; -*-
#
# Copyright (C) 2013 Stuart Prescott <stuart@debian.org>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""
Export a list of source and binary packages that are missing debtags
"""

import sys
import os
sys.path.insert(0, os.path.abspath('../../pylibs/'))
from cgi_helpers import *
from psycopg2 import connect
from re import split, sub


DATABASE = 'service=udd'
QUERY = """\
  SELECT source,
         string_agg(DISTINCT package, ' ' ORDER BY package) AS packages
    FROM packages
   WHERE package NOT IN
         (SELECT DISTINCT package
          FROM debtags)
         AND release = 'sid'
         AND distribution = 'debian'
GROUP BY source
ORDER BY source
"""

conn = connect(DATABASE)
cur = conn.cursor()
cur.execute(QUERY)
rows = cur.fetchall()
cur.close()
conn.close()

print_contenttype_header('text/plain')
print("""# Packages that have no debtags information in UDD
# Format:
#   source\tpackage1 package2 ...
#""")

for row in rows:
    print("%s\t%s" % (row[0], row[1]))
