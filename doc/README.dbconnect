Customization of the CGI database connection parameters
=======================================================

The CGI scripts' db connections have been unified to use 'service=udd'
instead of hardcoded parameters.

If you need to change the pg database connection (for example because you
wish to test a script locally, but do not have access to udd on ullmann.d.o),
consider not overwriting these in the script itself, but instead define
a pg connection service file [1] at ~/.pg_service.conf with the parameters.

NOTE This doesn't work for the ruby scripts at present, as `Sequel' does
NOTE not support it. Those connections are configured in `rlibs/udd-db.rb`
NOTE and will have to be manually overridden there, for now.

Example ~/.pg_service.conf to use the public udd mirror:
-----8<-----
# psql service definition file
# (INI file syntax, without spaces)
#
# udd-mirror
[udd]
host=udd-mirror.debian.net
port=5432
dbname=udd
user=udd-mirror
password=udd-mirror
# (Note: use ~/.pgpass for a private pw instead)
connect_timeout=10
application_name=uddcgi
sslmode=require
----->8-----

(Remember cgi's are run as the executing webserver user.
 Or use the system-wide file: `pg_config --sysconfdir`/pg_service.conf)

[1] https://www.postgresql.org/docs/current/static/libpq-pgservice.html
