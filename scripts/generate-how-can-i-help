#!/usr/bin/ruby
# This script generates http://udd.debian.org/how-can-i-help.json.gz which is
# used by the how-can-i-help package to list possible contributions to Debian.
# 
# encoding: US-ASCII

$:.unshift File.expand_path('../rlibs', File.dirname(__FILE__))
require 'udd-db'
require 'json'
require 'pp'
require 'digest/md5'

OFILE='/srv/udd.debian.org/udd/web/how-can-i-help.json.gz'
#if `hostname`.chomp == 'grep'
#  OFILE='/tmp/how-can-i-help.json.gz'
#end

DB = Sequel.connect(UDD_GUEST)

def dbget(q, *args)
  if $debug
    puts "<pre>#{q}</pre>"
    p args if not args.nil?
  end
  rows = DB[q].all.sym2str
  return rows
end

def system_r(s)
  system(s) or raise
end

class String
  def force_ascii
    self.encode!('US-ASCII', 'UTF-8', :invalid => :replace, :undef => :replace, :replace => '?')
  end
end

rows = dbget("select release from releases where role = 'testing'")
testing = rows[0]['release']

srcs = {}
dbget("select distinct package, source from packages_summary").each do |r|
  srcs[r['source']] ||= []
  srcs[r['source']] << r['package']
end

helpitems = []
dbget("select * from orphaned_packages").each do |r|
  hash = Digest::MD5.hexdigest("wnpp #{r['type']} #{r['source']} #{r['bug']}")
  next if srcs[r['source']].nil?
  helpitems << {
    'type' => 'wnpp',
    'hash' => hash,
    'wnpptype'=> r['type'],
    'source' => r['source'],
    'packages' => srcs[r['source']],
    'wnppbug' => r['bug'],
    'wnppdesc' => r['description'].nil? ? nil : r['description'].force_ascii
  }
end

pseudopkg = dbget("select package from pseudo_packages").map { |r| r['package'] }
pseudopkg -= ['mirrors', 'base', 'cdrom', 'wnpp', 'installation-reports', 'tech-ctte', 'general', 'project', 'spam', 'press', 'release-notes', 'upgrade-reports']

dbget("select id, source, package, title from bugs where (id in (select id from bugs_usertags where email='debian-qa@lists.debian.org' and tag='gift') or id in (select id from bugs_tags where tag='newcomer')) and status != 'done';").each do |r|
  hash = Digest::MD5.hexdigest("gift #{r['id']}")
  helpitems << {
    'type' => 'gift',
    'hash' => hash,
    'source' => r['source'],
    'package' => r['package'],
    'bug' => r['id'],
    'title' => r['title'].force_ascii,
    'pseudo-package' => pseudopkg.include?(r['package'])
  }
end

dbget("select id, source, package, title from bugs where (id in (select id from bugs_tags where tag='help')) and status != 'done';").each do |r|
  hash = Digest::MD5.hexdigest("help #{r['id']}")
  helpitems << {
      'type' => 'help',
      'hash' => hash,
      'source' => r['source'],
      'package' => r['package'],
      'bug' => r['id'],
      'title' => r['title'].force_ascii,
      'pseudo-package' => pseudopkg.include?(r['package'])
  }
end

dbget("""select distinct packages_summary.source, package, sync
      from packages_summary, migrations where distribution='debian' and release='sid'
      and package not in (select package from packages_summary where distribution='debian' and release='#{testing}')
      and packages_summary.source not in (select source from sources where distribution='debian' and release='#{testing}')
      and packages_summary.source = migrations.source and sync is not null").each do |r|
  hash = Digest::MD5.hexdigest("no-testing #{r['package']}")
  helpitems << {
    'type' => 'no-testing',
    'hash' => hash,
    'source' => r['source'],
    'package' => r['package']
  }
end

dbget("""select source, version, bugs, removal_time from testing_autoremovals").each do |r|
  hash = Digest::MD5.hexdigest("testing-autorm #{r['source']} #{r['version']} #{r['removal_time']}")
  helpitems << {
    'type' => 'testing-autorm',
    'hash' => hash,
    'source' => r['source'],
    'packages' => srcs[r['source']],
    'removal_time' => r['removal_time'],
    'bugs' => r['bugs'].nil? ? [] : r['bugs'].split(',')
  }
end

dbget(" select distinct ss.id, ss.source, ss.version, ss.title
        from sponsorship_requests ss, sources_uniq su
        where su.release = 'sid'
        and ss.source = su.source
        and su.version < ss.version").each do |r|
  hash = Digest::MD5.hexdigest("rfs #{r['source']} #{r['version']} #{r['id']}")
  helpitems << {
    'type' => 'rfs',
    'hash' => hash,
    'source' => r['source'],
    'packages' => srcs[r['source']],
    'version' => r['version'],
    'title' => r['title'],
    'id' => r['id']
  }
end


fd = File::new(OFILE+'.1', 'w')
JSON::dump(helpitems, fd)
fd.close

system_r("gzip < #{OFILE+'.1'} > #{OFILE+'.2'}")
system_r("mv #{OFILE+'.2'} #{OFILE}")
system_r("rm #{OFILE+'.1'}")
